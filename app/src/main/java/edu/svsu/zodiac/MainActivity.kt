package edu.svsu.zodiac

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * This class is the main activity of the app. It instantiates an arrayList of zodiacSigns and
 * populates each entry with the proper information. It then instantiates the recyclerView
 * adapter to use that arrayList. It also includes a function to display the detail page for
 * any clicked sign.
 */
class MainActivity : AppCompatActivity() {
    var dh: DataHandler? = null
    var signs = ArrayList<String>()
    /**
     * Sets up recyclerView to display zodiac signs, calls getSigns()
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dh = DataHandler(this)
        getSigns()
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        val signAdapter = ZodiacSignAdapter(this, signs!!)
        recyclerView.adapter = signAdapter
        recyclerView!!.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

    }

    /**
     * Calls function in DataHandler to get list of all signs in the database (used to populate
     * the recyclerView
     */
    private fun getSigns(){
        val cur = dh!!.getSigns()
        while(cur.moveToNext()){
            signs.add(cur.getString(0))
        }
    }
    /**
     * Function to call second activity to display selected sign in detail. Queries the database
     * for selected sign, then passes relevant information to the activity
     */
    fun displaySign(pos: Int) {
        val cur = dh!!.searchSign(signs[pos])
        val myIntent = Intent(this, Sign::class.java)
        cur.moveToNext()
        myIntent.putExtra("SIGN_NAME", cur.getString(1))
        myIntent.putExtra("SIGN_DESCRIPTION", cur.getString(2))
        myIntent.putExtra("SIGN_SYMBOL", cur.getString(3))
        myIntent.putExtra("SIGN_MONTH", cur.getString(4))
        startActivity(myIntent)
    }
}