package edu.svsu.zodiac

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

/**
 * This class is used to populate the recyclerView in activity_main with the contents of
 * an arrayList of zodiacSigns
 */
class ZodiacSignAdapter(
    private val mainActivity: MainActivity,
    private val zodiacList: ArrayList<String>
) :
    RecyclerView.Adapter<ZodiacSignAdapter.ListItemHolder>() {

    /**
     * This class is used to set up each listItem used in the recyclerView
     */
    inner class ListItemHolder(view: View) :
        RecyclerView.ViewHolder(view),
        View.OnClickListener {

        /**
         * Name field in the list itetm
         */
        internal var name = view.findViewById<TextView>(R.id.tvZodiacSign)

        /**
         * Set the list item to clickable, and point onClick to this class
         */
        init {
            view.isClickable = true
            view.setOnClickListener(this)
        }

        /**
         * Call displaySign in mainActivity to display details of clicked sign
         */
        override fun onClick(view: View) {
            mainActivity.displaySign(adapterPosition)
        }
    }

    /**
     * Selects which view is used to hold the list item
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.listitem, parent, false)
        return ListItemHolder(itemView)
    }

    /**
     * Set how the recyclerView can get a count of items it contains
     */
    override fun getItemCount(): Int {
        return zodiacList.size
    }

    /**
     * Sets what happens when a view is bound to a specific holder
     */
    override fun onBindViewHolder(holder: ListItemHolder, position: Int) {
        val sign = zodiacList[position]
        holder.name.text = sign
    }
}