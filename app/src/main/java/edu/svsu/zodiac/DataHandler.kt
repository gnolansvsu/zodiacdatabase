package edu.svsu.zodiac

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * This class handles the data needs of the new and improved Zodiac app
 */
class DataHandler(context: Context) {
    /**
     * Reference to database
     */
    private val db: SQLiteDatabase

    /**
     * Initialize the datahandler. First get a itialize the
     * inner class CustomSQLiteOpenHandler. Then check if database table is empty. If
     * so delete everything from table (probably unnecessary), then populate the
     * table with the entries that were in the static list
     */
    init {
        val helper = CustomSQLiteOpenHelper(context)
        db = helper.writableDatabase
        //insert necessary values into db if empty
        if (getTableSize() < 1) {
            deleteAll()
            insert(
                "Taurus",
                "Known for being reliable, practical, ambitious and sensual.",
                "Bull",
                "May"
            )
            insert("Gemini", "Gemini-born are clever and intellectual.", "Twins", "June")
            insert("Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July")
            insert(
                "Leo",
                "Warm, action-oriented and driven by the desire to be loved and admired.",
                "Lion",
                "August"
            )
            insert(
                "Virgo",
                "Methodical, meticulous, analytical and mentally astute.",
                "Virgin",
                "September"
            )

            insert(
                "Libra",
                "Librans are famous for maintaining balance and harmony.",
                "Scales",
                "October"
            )

            insert("Scorpio", "Strong willed and mysterious.", "Scorpion", "November")
            insert("Sagittarius", "Born adventurers.", "Archer", "December")
            insert(
                "Capricorn",
                "The most determined sign in the Zodiac.",
                "Goat",
                "January"
            )

            insert("Aquarius", "Humanitarians to the core", "Water Bearer", "February")
            insert("Pisces", "Proverbial dreamers of the Zodiac.", "Fish", "March")
        }
    }

    /**
     * This function returns the results of a query that selects each sign name from the
     * database
     */
    fun getSigns(): Cursor{
        val query = "SELECT DISTINCT " + TABLE_ROW_NAME + " FROM " + TABLE_N_D_S_M + ";"
        return db.rawQuery(query, null)
    }

    /**
     * This function deletes all entries from the table containing the sign data
     */
    fun deleteAll(){
        val query = "DELETE FROM " + TABLE_N_D_S_M + ";"
        db.execSQL(query)
    }

    /**
     * This function returns the number of entries are in the table
     */
    private fun getTableSize(): Int {
        val query = "SELECT COUNT(*) FROM " + TABLE_N_D_S_M + ";"
        val cursor = db.rawQuery(query, null)
        cursor.moveToFirst()
        return cursor.getInt(0)
    }

    /**
     * This function inserts a new record into the table
     */
    private fun insert(sign: String, description: String, symbol: String, month: String) {
        val query = "INSERT INTO " + TABLE_N_D_S_M + " (" +
                TABLE_ROW_NAME + ", " +
                TABLE_ROW_DESC + ", " +
                TABLE_ROW_SYMBOL + ", " +
                TABLE_ROW_MONTH + ") " +
                "VALUES (" +
                "'" + sign + "', '" +
                description + "', '" +
                symbol + "', '" +
                month + "');"
        db.execSQL(query)
    }

    /**
     * This function would be used to delete a single entry from the table. It isn't used
     * in this project, but was good practice to write
     */
    fun delete(sign: String) {
        val query = "DELETE FROM " + TABLE_N_D_S_M + " WHERE " +
                TABLE_ROW_NAME + " = '" + sign + "';"
        db.execSQL(query)
    }

    /**
     * This function would return all entries in the table. Again, not used, but good practice
     */
    fun selectAll(): Cursor {
        return db.rawQuery("SELECT * FROM " + TABLE_N_D_S_M, null)
    }

    /**
     * This function is used to search for a specific sign by name in the database. It returns
     * all tuples associated with that name
     */
    fun searchSign(sign: String): Cursor {
        val query = "SELECT " +
                TABLE_ROW_ID + ", " +
                TABLE_ROW_NAME + ", " +
                TABLE_ROW_DESC + ", " +
                TABLE_ROW_SYMBOL + ", " +
                TABLE_ROW_MONTH +
                " FROM " +
                TABLE_N_D_S_M +
                " WHERE " +
                TABLE_ROW_NAME + " = '" +
                sign + "';"
        return db.rawQuery(query, null)
    }

    /**
     * Singleton object to hold static variables
     */
    companion object {
        const val TABLE_ROW_ID = "_id"
        const val TABLE_ROW_NAME = "sign"
        const val TABLE_ROW_DESC = "description"
        const val TABLE_ROW_SYMBOL = "symbol"
        const val TABLE_ROW_MONTH = "month"

        private const val DB_NAME = "zodiac_db"
        private const val DB_VERSION = 1
        private const val TABLE_N_D_S_M = "sign_description_symbol_month"
    }

    /**
     * Inner class that is used to handle opening a connection to the database
     */
    private inner class CustomSQLiteOpenHelper(context: Context) :
        SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
        /**
         * Function called to create the database
         */
        override fun onCreate(db: SQLiteDatabase) {
            val newTableQueryString = ("CREATE TABLE " +
                    TABLE_N_D_S_M + "(" +
                    TABLE_ROW_ID +
                    " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    TABLE_ROW_NAME + " TEXT NOT NULL, " +
                    TABLE_ROW_DESC + " TEXT NOT NULL, " +
                    TABLE_ROW_SYMBOL + " TEXT NOT NULL, " +
                    TABLE_ROW_MONTH + " TEXT NOT NULL);")
            db.execSQL(newTableQueryString)
        }

        /**
         * Function called to upgrade the database. Upgrading is not implemented for this project
         */
        override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
            //leaving empty for this assignment
        }

    }
}