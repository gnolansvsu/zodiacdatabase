package edu.svsu.zodiac

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 * Activity to display specific details of a selected sign
 */
class Sign : AppCompatActivity() {

    /**
     * Sets layout to display, and populates the layout with proper details
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)
        findViewById<TextView>(R.id.txtName).text = intent.getStringExtra("SIGN_NAME")
        findViewById<TextView>(R.id.txtDescription).text = intent.getStringExtra("SIGN_DESCRIPTION")
        findViewById<TextView>(R.id.txtSymbol).text = intent.getStringExtra("SIGN_SYMBOL")
        findViewById<TextView>(R.id.txtMonth).text = intent.getStringExtra("SIGN_MONTH")
    }
}